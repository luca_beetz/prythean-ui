import React from 'react';
import { PropTypes } from 'prop-types';

import DraggableCard from 'containers/DraggableCard/DraggableCard';
import PlayerCardModel from 'models/PlayerCard';

import styles from './Hand.module.scss';

const Hand = ({cards, isPlayersTurn}) => {
    const cardList = cards.map((card, index) => {
        return(
            <DraggableCard
                card={card}
                isPlayersTurn={isPlayersTurn}
                key={index}
            />
        );
    });

    return(
        <div className={styles.Hand}>
            { cardList }
        </div>
    );
}

Hand.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.instanceOf(PlayerCardModel)).isRequired,
    isPlayersTurn: PropTypes.bool.isRequired,
}

export default Hand;