import React from 'react';
import PropTypes from 'prop-types';

import PlayerCardModel from 'models/PlayerCard';

import styles from './Card.module.scss';

import cardImage from '../../res/base_card.png';

const Card = ({card}) => {
    return(
        <div className={styles.Card}>
            <img className={styles.CardImage} src={cardImage} alt="CardImage" draggable="false"></img>
            <div className={styles.CardMana}>{card.manaCost}</div>
            <div className={styles.CardTitle}>{card.title}</div>
            <div className={styles.CardAttack}>{card.attack}</div>
            <div className={styles.CardHealth}>{card.health}</div>
        </div>
    );
}

Card.propTypes = {
    card: PropTypes.instanceOf(PlayerCardModel),
};

export default Card;