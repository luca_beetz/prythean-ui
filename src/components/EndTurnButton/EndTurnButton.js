import React from 'react';
import { PropTypes } from 'prop-types';

import styles from './EndTurnButton.module.scss';

const EndTurnButton = ({isPlayersTurn, callback}) => {
    const buttonText = isPlayersTurn ? "End turn" : "Enemy's turn";
    const activeStyle = isPlayersTurn ? styles.Active : styles.Inactive;

    return(
        <div className={styles.EndTurnButton + ' ' + activeStyle} onClick={callback}>
            <p className={styles.ButtonText}>{buttonText}</p>
        </div>
    )
}

EndTurnButton.propTypes = {
    isPlayersTurn: PropTypes.bool.isRequired,
}

export default EndTurnButton;