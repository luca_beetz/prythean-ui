import React from 'react';
import { PropTypes } from 'prop-types';

import Hand from 'components/Hand/Hand';
import TargetableHero from 'containers/TargetableHero/TargetableHero';
import BoardSide from 'components/BoardSide/BoardSide';
import BoardSideDropTarget from 'containers/BoardSideDropTarget/BoardSideDropTarget';

import PlayerModel from 'models/Player';

import styles from './Player.module.scss';

const Player = ({player, isPlayersTurn, callbacks}) => {
    return(
        <div className={styles.Player}>
            <BoardSide>
                <BoardSideDropTarget
                    minions={player.activeMinions}
                    isPlayersTurn={isPlayersTurn}
                    callbacks={callbacks}
                />
            </BoardSide>
            <div className={styles.PlayerHero}>
                <TargetableHero hero={player.hero} />
            </div>
            <div className={styles.PlayerHandWrapper}>
                <Hand cards={player.handCards} isPlayersTurn={isPlayersTurn} />
            </div>
        </div>
    );
}

Player.propTypes = {
    player: PropTypes.instanceOf(PlayerModel).isRequired,
    isPlayersTurn: PropTypes.bool.isRequired,
}

export default Player;