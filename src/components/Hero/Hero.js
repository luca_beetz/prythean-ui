import React from 'react';
import PropTypes from 'prop-types';

import HeroModel from 'models/Hero';
import styles from './Hero.module.scss';

import heroImage from './../../res/monster.png';

const Hero = ({hero}) => {
    return(
        <div className={styles.Hero}>
            <div className="Content">
                <img src={heroImage} alt="Hero" className={styles.HeroImage} draggable="false"></img>
                <p className={styles.HeroName}>{hero.name}</p>
                <p className={styles.HeroHealth}>Health: {hero.health}</p>
                <p className={styles.HeroMana}>Mana: {hero.currentMana} / {hero.maxMana}</p>
            </div>
        </div>
    );
}

Hero.propTypes = {
    hero: PropTypes.instanceOf(HeroModel)
};

export default Hero;