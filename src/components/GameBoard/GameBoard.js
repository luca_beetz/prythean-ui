import React from 'react';
import { PropTypes } from 'prop-types';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import PlayerSide from 'components/PlayerSide/PlayerSide';
import Player from 'components/Player/Player';
import Opponent from 'components/Opponent/Opponent';
import EndTurnButton from 'components/EndTurnButton/EndTurnButton';

import GameModel from 'models/Game';

import styles from './GameBoard.module.scss';

const GameBoard = ({game, callbacks}) => {
    return(
        <DndProvider backend={HTML5Backend}>
            <div className={styles.GameBoard}>
                <PlayerSide className="PlayerSideOfEnemy">
                    <Opponent opponent={game.opponent} callbacks={callbacks} />
                </PlayerSide>
                <PlayerSide className="PlayerSideOfPlayer">
                    <Player player={game.player} isPlayersTurn={game.isPlayersTurn} callbacks={callbacks} />
                </PlayerSide>
                <EndTurnButton isPlayersTurn={game.isPlayersTurn} callback={callbacks.endTurnButton} />
            </div>
        </DndProvider>
    )
}

GameBoard.propTypes = {
    game: PropTypes.instanceOf(GameModel),
    callback: PropTypes.objectOf(PropTypes.func),
};

export default GameBoard;