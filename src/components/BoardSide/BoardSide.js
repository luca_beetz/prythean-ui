import React from 'react';
import PropTypes from 'prop-types';

import styles from './BoardSide.module.scss';

const BoardSide = ({children}) => {
    return(
        <div className={styles.BoardSide}>
            { children }
        </div>
    )
}

BoardSide.propTypes = {
    children: PropTypes.element.isRequired,
}

export default BoardSide;