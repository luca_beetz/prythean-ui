import React from 'react';
import { PropTypes } from 'prop-types';

import MinionModel from 'models/Minion';
import styles from './Minion.module.scss';

import cardImage from '../../res/base_card.png';

const Minion = ({minion}) => {
    const card = minion.details;

    return(
        <div className={styles.Minion}>
            <img className={styles.MinionImage} src={cardImage} alt="MinionImage" draggable="false"></img>
            <div className={styles.MinionMana}>{card.manaCost}</div>
            <div className={styles.MinionTitle}>{card.title}</div>
            <div className={styles.MinionAttack}>{card.attack}</div>
            <div className={styles.MinionHealth}>{card.health}</div>
        </div>
    )
}

Minion.propTypes = {
    minion: PropTypes.instanceOf(MinionModel),
}

export default Minion;