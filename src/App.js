import React from 'react';

import styles from './App.module.scss';

import SocketWrapper from './containers/SocketWrapper/SocketWrapper';

function App() {
  return (
    <div className={styles.App}>
      <SocketWrapper />
    </div>
  );
}

export default App;
