import React, { Component } from 'react';
import { DragSource } from 'react-dnd';

import Card from 'components/Card/Card';

class DraggableCard extends Component {
    render() {
        const { connectDragSource, isDragging, card } = this.props;

        return connectDragSource(
            <div style={{ display: isDragging ? 'none' : undefined }}>
                <Card card={card} />
            </div>
        )
    }
}

const cardDragSource = {
    canDrag(props) {
        return props.isPlayersTurn;
    },
    beginDrag(props) {
        return {
            card: props.card,
            source: 'PLAYER',
            target: 'PLAYER',
        }
    }
};

const collect = (connect, monitor) => {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging(),
    };
}

export default DragSource('CARD', cardDragSource, collect)(DraggableCard);