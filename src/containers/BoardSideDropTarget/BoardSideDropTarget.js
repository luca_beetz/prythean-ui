import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';

import MinionsOnBoard from 'components/MinionsOnBoard/MinionsOnBoard';
import MinionDropTarget from 'containers/MinionDropTarget/MinionDropTarget';

class BoardSideDropTarget extends Component {
    render() {
        const {
            isPlayersTurn,
            minions,
            connectDropTarget,
            callbacks,
        } = this.props;

        const minionTargets = minions.map((minion, index) => (
            <MinionDropTarget
                isPlayersTurn={isPlayersTurn}
                minion={minion}
                key={index}
                callbacks={callbacks}
            />
        ));

        return connectDropTarget(
            <div style={{ height: '100%' }}>
                <MinionsOnBoard>
                    { minionTargets }
                </MinionsOnBoard>
            </div>
        );
    }
}

const boardDropTarget = {
    canDrop(props) {
        return true;
    },
    drop(props, monitor, component) {
        console.log("Dropping card");
        console.log(monitor.getItem());
        props.callbacks.playCard(monitor.getItem().card.index);
    }
}

function collect(connect) {
    return { connectDropTarget: connect.dropTarget() };
}

export default DropTarget('CARD', boardDropTarget, collect)(BoardSideDropTarget);