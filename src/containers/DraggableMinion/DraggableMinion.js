import React, { Component } from 'react';
import { DragSource } from 'react-dnd';

import Minion from 'components/Minion/Minion';

class DraggableMinion extends Component {
    render() {
        const { connectDragSource, isDragging, minion } = this.props;

        return connectDragSource(
            <div style={{ display: isDragging ? 'none' : '', margin: '0 10px' }}>
                <Minion minion={minion} />
            </div>
        );
    }
}

const minionDragSource = {
    beginDrag(props) {
        return {
            card: props.minion,
        };
    },
    canDrag(props) {
        return props.isPlayersTurn;
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging(),
    };
}

export default DragSource('MINION', minionDragSource, collect)(DraggableMinion);