class CardInfo {
    constructor(title, imageUrl, manaCost, health, attack) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.manaCost = manaCost;
        this.health = health;
        this.attack = attack;
    }
}

export default CardInfo;