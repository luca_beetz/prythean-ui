class HeroModel {
    constructor(name, health, currentMana, maxMana) {
        this.name = name;
        this.health = health;
        this.currentMana = currentMana;
        this.maxMana = maxMana;
    }
}

export default HeroModel;