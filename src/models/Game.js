class GameModel {
    constructor(player, opponent, turnCount, isPlayersTurn) {
        this.player = player;
        this.opponent = opponent;
        this.turnCount = turnCount;
        this.isPlayersTurn = isPlayersTurn;
    }
}

export default GameModel;