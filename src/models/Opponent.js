class OpponentModel {
    constructor(handCardCount, activeMinions, hero) {
        this.handCardCount = handCardCount;
        this.activeMinions = activeMinions;
        this.hero = hero;
    }
}

export default OpponentModel;